﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bla1.Models
{
  public class Input : Tag
  {
    public String GroupName = "";
    private Input()
    {
      Name = "input";
      InnerText = string.Empty;
      AddAttribute("type", InputTagType.text.ToString());
    }
    public Input(InputTagType type)
    {
      Name = "input";
      InnerText = string.Empty;
      AddAttribute("type", type.ToString());
    }
    public override void RemoveAttribute(string key)
    {
      if (key.Equals("type"))
      {
        Console.WriteLine("You cannot delete type attribute in an InputTag");
        return;
      }
      base.RemoveAttribute(key);
    }
    protected override void CloseEndTag(StringBuilder name)
    {
      name.Append("\n");
    }
    protected override void CloseOpenTag(StringBuilder name)
    {
      name.Append("/>");
    }

  }
}
