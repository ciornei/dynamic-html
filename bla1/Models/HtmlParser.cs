﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bla1.Models
{
  public class HtmlParser
  {
    public List<Tag> AvailableParents = new List<Tag>();


    public HtmlParser()
    {

    }

    public string StartParser(string content)
    {
      int StartIndexTag, EndIndexTag, temporar;
      string TagText;
      string innerText;
      List<string> TagElements = new List<string>();
      List<string> TagAtributes = new List<string>();

      StartIndexTag = content.IndexOf("<");
      EndIndexTag = content.IndexOf(">");
      TagText = content.Substring(StartIndexTag + 1, EndIndexTag - 1);
      TagElements = TagText.Split(' ').ToList();

      Tag tag = new Tag(TagElements[0]);

      foreach (var item in TagElements.Skip(1))
      {
        TagAtributes = item.Split('=').ToList();
        tag.AddAttribute(TagAtributes[0], TagAtributes[1]);
      }
      if (AvailableParents.Count != 0)
      {
        AvailableParents.Last().AddChild(tag);
      }
      AvailableParents.Add(tag);
      
      content.Remove(0, EndIndexTag);

      try
      {
        switch (content.ElementAt(0).ToString())
        {
          case "/":
            switch (content.ElementAt(2).ToString())
            {
              case "<":
                AvailableParents.Remove(AvailableParents.Last());

                switch (content.ElementAt(3).ToString())
                {
                  case "/":

                    AvailableParents.Remove(AvailableParents.Last());
                    EndIndexTag = content.IndexOf(">");
                    content.Remove(0, EndIndexTag - 1);
                    StartParser(content);
                    break;
                  default:
                    content.Remove(0, 2);
                    StartParser(content);

                    break;
                }

                break;
              default:

                EndIndexTag = content.IndexOf("<");
                innerText = content.Substring(2, EndIndexTag - 1);
                AvailableParents.Last().InnerText = innerText;
                AvailableParents.Remove(AvailableParents.Last());
                content.Remove(0, EndIndexTag);
                StartParser(content);
                break;
            }
            break;
          default:

            switch (content.ElementAt(2).ToString())
            {
              case "<":
                switch (content.ElementAt(3).ToString())
                {
                  case "/":
                    AvailableParents.Remove(AvailableParents.Last());
                    break;
                  default:
                    content.Remove(0, 2);
                    StartParser(content);
                    break;
                }
                break;
              default:
                EndIndexTag = content.IndexOf("<");
                innerText = content.Substring(2, EndIndexTag - 1);
                AvailableParents.Last().InnerText = innerText;
                content.Remove(0, 2);
                StartParser(content);
                break;
            }

            break;
        }

      }
      catch (Exception ex)
      {
        Console.WriteLine("done -- s-a terminat stringul (maybe?...)");
      }


      //switch (content.ElementAt(2).ToString())
      //{
      //  case "<":
      //    switch(content.ElementAt(3).ToString())
      //    {
      //      case "/":
      //        AvailableParents.RemoveAt(AvailableParents.Count());
      //        break;
      //      default:

      //        break;
      //    }
      //    break;
      //  default:

      //    break;
      //}

      

      return "";
    }



  }
}
