﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bla1.Models
{
  public class RadioInput : Input
  {
    public RadioInput() : base(InputTagType.radio)
    {
      //Name = "input";
      //ModifyAttribute("type", InputTagType.radio.ToString());
    }

    public RadioInput(string text) : base(InputTagType.radio)
    {
      //Name = "input";
      //ModifyAttribute("type", InputTagType.radio.ToString());
      this.InnerText = text + "</br>";
    }

    public void AddGroupName(string group)
    {
      this.AddAttribute("name", group);
    }
    public void AddText(string text)
    {
      this.InnerText = text + "</br>";
    }

    public void Checked()
    {
      this.AddAttribute("checked", "");
    }
  }
}
