﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bla1.Models
{

  public class Tag
  {
    public string Name { get; set; }
    public string InnerText { get; set; }
    public List<Tag> Children = new List<Tag>();
    public Dictionary<string, string> Attributes = new Dictionary<string, string>();
    static int TabCount = 0;
    public Tag()
    {
      InnerText = string.Empty;
    }
    public Tag(string Name)
    {
      this.Name = Name;
      InnerText = string.Empty;
    }
    public Tag(string Name, string InnerText)
    {
      this.Name = Name;
      this.InnerText = InnerText;
    }
    public virtual void AddChild(Tag child)
    {
      Children.Add(child);
    }
    public virtual void AddAttribute(string key, string value)
    {
      if (!string.IsNullOrEmpty(key))
      {
        Attributes.Add(key, value);
      }
    }
    public virtual void RemoveAttribute(string key)
    {
      if (!string.IsNullOrEmpty(key) && Attributes.ContainsKey(key))
      {
        Attributes.Remove(key);
      }
    }
    
    public void ModifyAttribute(string key, string value)
    {
      if (!string.IsNullOrEmpty(key) && Attributes.ContainsKey(key))
      {
        Attributes[key] = value;
      }
      else
      {
        AddAttribute(key, value);
      }
    }


    protected virtual void CloseEndTag(StringBuilder name)
    {
      name.Append("</" + Name + ">\n");
    }
    protected virtual void CloseOpenTag(StringBuilder name)
    {
      name.Append(">");
    }
    private StringBuilder RenderInternal()
    {
      var wholeString = new StringBuilder();
      for (int i = 0; i < TabCount; i++)
      {
        wholeString.Append("\t");
      }
      wholeString.Append("<" + Name);
      if (Attributes.Count > 0)
      {
        foreach (var item in Attributes)
        {
          wholeString.Append(" " + item.Key + "=\"" + item.Value + "\"");
        }
      }
      CloseOpenTag(wholeString);
      
      if (!string.IsNullOrEmpty(InnerText))
      {
        wholeString.Append(InnerText);
      }
      else
      {
        wholeString.Append("\n");
      }
      if (Children.Count > 0)
      {
        foreach (var child in Children)
        {
          TabCount++;
          wholeString.Append(child.RenderInternal());
          TabCount--;
        }
      }
      if (string.IsNullOrEmpty(InnerText))
      {
        for (int i = 0; i < TabCount; i++)
        {
          wholeString.Append("\t");
        }
      }
      CloseEndTag(wholeString);
      //wholeString.Append("</" + Name + ">\n");
      return wholeString;
    }
    public string Render()
    {
      return RenderInternal().ToString();
    }

  }
}
