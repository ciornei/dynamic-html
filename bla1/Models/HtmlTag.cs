﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bla1.Models
{
  public class HtmlTag : Tag
  {
    public HtmlTag()
    {
      Name = "html";
      InnerText = string.Empty;
    }
    public override void AddChild(Tag child)
    {
      if (child is HeadTag && !Children.Any(x => x is HeadTag))
      {
        Children.Add(child);

      }
      else if (child is BodyTag && !Children.Any(x => x is BodyTag))
      {
        Children.Add(child);
      }
      else if (child is HeadTag)
      {
        Console.WriteLine("HtmlTag already has a HeadTag");
        Environment.Exit(0);
      }
      else if (child is BodyTag)
      {
        Console.WriteLine("HtmlTag already has a BodyTag");
        Environment.Exit(0);
      }
    }
  }
}
